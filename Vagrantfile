# -*- mode: ruby -*-
# vi: set ft=ruby :

utilBoxImage = "ubuntu/jammy64"
esBoxImage = "ubuntu/jammy64"
utilMemory = 2048
esMemory = 8192 
elasticNumHosts = 3
elasticAddressStart = '192.168.56.2'
utilAddress = '192.168.56.10'

### Utility system
Vagrant.configure("2") do |config|
  hostname = 'util-01'

  config.vm.define hostname do |hostconfig|
    hostconfig.vm.box = utilBoxImage
    hostconfig.vm.hostname = hostname
    hostconfig.vm.network "private_network", ip: utilAddress
    hostconfig.vm.network "forwarded_port", guest: 80, host: 8080
    hostconfig.vm.network "forwarded_port", guest: 5601, host: 8081 
#    hostconfig.vm.network "forwarded_port", guest: 9000, host: 9090
    hostconfig.vm.synced_folder ".", "/vagrant"
    hostconfig.vm.provider :virtualbox do |vb|
      vb.customize ["modifyvm", :id, "--memory", utilMemory.to_s, "--cpus", 2]
    end
    hostconfig.vm.provision "file", source: "./vagrant_insecure_private_key", destination: "/home/vagrant/.ssh/id_rsa"
    hostconfig.vm.provision "file", source: "./ssh_config", destination: "/home/vagrant/.ssh/config"
    hostconfig.vm.provision "file", source: "./etc_hosts", destination: "/tmp/etc_hosts"
    hostconfig.vm.provision "shell", inline: <<-SHELL
      apt update
      apt install -y git ansible
      mkdir -p /home/vagrant/git
      chmod 777 /home/vagrant/git
      ln -s /vagrant /home/vagrant/git/oelk
      chmod 600 /home/vagrant/.ssh/id_rsa
      ssh-keygen -y -f /home/vagrant/.ssh/id_rsa >> /home/vagrant/.ssh/authorized_keys
      cat /tmp/etc_hosts >> /etc/hosts
      echo 'DONE'
      exit 0
    SHELL
  end
end

### Elastic cluster
Vagrant.configure("2") do |config|

  (1..elasticNumHosts).each do |i|

    hostname = 'es-0' + i.to_s
    ipAddress = elasticAddressStart + i.to_s

    config.vm.define hostname do |hostconfig|
      hostconfig.vm.box = esBoxImage
      hostconfig.vm.hostname = hostname
      hostconfig.vm.network :private_network, ip: ipAddress
      hostconfig.vm.network "forwarded_port", guest: 9200, host: '920' + i.to_s 
      hostconfig.vm.provider :virtualbox do |vb|
        vb.customize ["modifyvm", :id, "--memory", esMemory.to_s, "--cpus", 1]
      end
      hostconfig.vm.provision "file", source: "./vagrant_insecure_private_key", destination: "/home/vagrant/.ssh/id_rsa"
      hostconfig.vm.provision "file", source: "./ssh_config", destination: "/home/vagrant/.ssh/config"
      hostconfig.vm.provision "file", source: "./etc_hosts", destination: "/tmp/etc_hosts"
      hostconfig.vm.provision "shell", inline: <<-SHELL
        apt update
        chmod 600 /home/vagrant/.ssh/id_rsa
        ssh-keygen -y -f /home/vagrant/.ssh/id_rsa >> /home/vagrant/.ssh/authorized_keys
        cat /tmp/etc_hosts >> /etc/hosts
        echo 'DONE'
        exit 0
      SHELL
    end
  end
end
