# oelk

Oracle ELK Coding Challenge public repo 

## Setup

1) Install vagrant on your system
2) Install VirtualBox on your system
3) Run ./bin/create-oelk.sh

## Caveats

### VirtualBox private network
This is set for a VirtualBox private network that is 192.168.56.1/24

### IP's are hard coded
No DNS, going old school with host files

### Elasticsearch security is disables
Not secure in any way at all

### No tuning on the Elastic hosts
Pretty much stock Ubuntu and that is all