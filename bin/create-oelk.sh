#! /bin/bash

declare -a requiredBins=( vagrant VBoxManage )
declare -a requiredBoxImages=( generic/alpine38 ubuntu/jammy64 )

echo -e "Starting up Jasun's Oracle ELK Challange...\n"

for filename in "${requiredBins[@]}"; do
  if command -v $filename &>/dev/null; then
    echo "  OK : found $filename"
  else
    echo "  ERROR : missing $filename"
    exit 1
  fi
done


for image in "${requiredBoxImages[@]}"; do
  echo -e "\nCheck to see if $image is installed...\n"

  if vagrant box list | grep -q "$image"; then
    echo -e "  OK : found $image."
  else
    echo -e "Installing $image for VirtualBox...\n"
    vagrant box add --provider virtualbox $image
    if [ $? -eq 0 ]; then
      echo -e "OK : $image installed.\n"
    else
      echo -e "ERROR : $image failed to install.\n"
      exit 1
    fi
  fi
done

echo -e "\nCheck to see if vagrant plugin vagrant-disksize is installed...\n"

if vagrant plugin list | grep -q "vagrant-disksize"; then
  echo -e "  OK : found vagrant plugin vagrant-disksize\n"
else
  echo -e "Installing vagrant plugin vagrant-disksize...\n"
  vagrant plugin install vagrant-disksize
  if [ $? -eq 0 ]; then
    echo -e "OK : vagrant plugin vagrant-disksize installed.\n"
  else
    echo -e "ERROR : vagrant plugin vagrant-disksize failed to install.\n"
    exit 1
  fi
fi

echo -e "Start up OELK..." 
vagrant up

